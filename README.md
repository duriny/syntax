# syntax

A Vim 8+ package containing the syntax packs I use

## Installation

To install `syntax`, clone [this](https://git.envs.net/duriny/syntax) repo under
`~/.vim/pack`:

```sh
# clone the package
git clone https://git.envs.net/duriny/syntax ~/.vim/pack/syntax

# initialize
cd ~/.vim/pack/syntax
git submodule update --init --recursive
```

...or, if you store your Vim configuration in git, you can add `syntax` as a
submodule:

```sh
# add the package as a submodule
git submodule add --branch main https://git.envs.net/duriny/syntax ~/.vim/pack/syntax

# initialize the package submodule (and it's own submodules)
git submodule update --init --recursive

# commit the new package
git commit -m "Added syntax package" ~/.vim/pack/syntax
```

## Updating

If you cloned `syntax` on its own, just `cd ~/.vim/pack/syntax && git pull` to
update the package. Otherwise, if you added `syntax` as a submodule, just run:

```sh
# update the syntax plugins
git submodule update --recursive --remote ~/.vim/pack/syntax

# commit the update
git commit -m "Updated syntax" ~/.vim/pack/syntax
```

## Usage

Once you have `syntax` installed, you can use any of the plugins included, not
that you may need to call `packadd {plugin}` in your `~/.vimrc`.

## Hacking

### Why is {plugin} missing/included?

I compiled `syntax` from plugins that I use everyday, if you think that your
favourite plugin is a good fit, send a PR, I'd love to see it.

### How can I add {plugin}?

Add the new plugin as a submodule of `syntax`, replace `{branch}` with the
main/master branch name of the upstream source, `{repo}` with the upstream clone
path, and `{plugin}` with the name of the plugin.

```sh
git submodule add --branch "{branch}" "{repo}" "opt/{plugin}"
git commit -am "Add {plugin}"
```

## Licensing

This package is distributed under the terms of the [Unlicense](LICENSE), and as
such is released into the public domain.

Note: *Each plugin is licensed by its creator, this repo only references them,
see the upstream source for licensing terms.*

